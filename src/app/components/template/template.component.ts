import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Validacion } from 'src/app/interfaces/validacion'


@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {
  
  datos: Validacion = {
    correo: '',
    tramites: 0 ,
    descripcion: ''
  }

  constructor() { }

  ngOnInit(): void {
  }

  guardar():void{
    console.log(this.datos.correo);
    console.log(this.datos.tramites);
    console.log(this.datos.descripcion);
  } 
}
